<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'mark'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_cars', 'cars_id', 'user_id');
    }
}
