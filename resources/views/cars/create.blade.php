@extends('layouts.app')

@section('title', 'Добавить машину')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <form method="POST" action="{{route('cars.store')}}">
                @csrf
                <div class="mb-3">
                    <label for="car-title" class="form-label">Марка</label>
                    <input type="text" name="mark" class="form-control" id="car-title" placeholder="Добавить машину">
                </div>
                <button type="submit" class="btn btn-primary">Добавить машину</button>
            </form>
        </div>
    </div>
@endsection
