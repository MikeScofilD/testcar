@extends('layouts.app')

@section('title', 'Изменить машину')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <form method="POST" action="{{route('cars.update', $car->id)}}">
                @csrf
                @method('PATCH')
                <div class="mb-3">
                    <label for="car-title" class="form-label">Марка</label>
                    <input type="text" name="mark" value="{{$car->mark}}" class="form-control" id="car-title" placeholder="Добавить машину">
                </div>
                <button type="submit" class="btn btn-primary">Изменить машину</button>
            </form>
        </div>
    </div>
@endsection
