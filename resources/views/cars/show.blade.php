@extends('layouts.app')

@section('title', 'Просмотр машины')

@section('content')
<div class="card">
    <div class="card-body">
      <h5 class="card-title">{{$car->mark}}</h5>
    </div>
  </div>
@endsection
