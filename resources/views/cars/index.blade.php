@extends('layouts.app')

@section('title', 'Все машины')

@section('content')
    <a href="{{route('cars.create')}}" class="btn btn-success">Add Car</a>
    @if (session()->get('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <table class="table mt-3">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Марка</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cars as $car)
                <tr>
                    <th scope="row">{{$car->id}}</th>
                    <td>{{$car->mark}}</td>
                    <td class="table-buttons">
                        <a href="{{route('cars.show', $car->id)}}" class="btn btn-success">View</a>
                        <a href="{{route('cars.edit', $car->id)}}" class="btn btn-primary">Edit</a>
                        <form action="{{route('cars.destroy',  $car->id)}}" type="submit" method="POST">
                            @csrf
                            @method('DELETE')
                            <button href="" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
